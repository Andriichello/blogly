# README

This is a simple blog called ***Blogly***.

Available actions:
1. Log In
2. Register
3. Continue as a Guest (every guest is under one account)
4. Create Post
5. Edit Post (if you are currently in post's creator account)
6. Delete Post (if you are currently in post's creator account)
7. Comment Post
8. Search Posts (searches by title, article and creator)

Screen Recording: https://drive.google.com/open?id=14Xupue2QCFpaN7dCUri4TlNFMnwd_bh7
